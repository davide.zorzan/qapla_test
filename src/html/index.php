<?php
$tcode = $_GET['code'];//'XP091570490FR';
$host='www.chronopost.fr';
$url = "https://$host/tracking-no-cms";
//$url = "$urlroot/suivi-page?listeNumerosLT=$tcode&langue=en";

$ajaxUrl = "$url/suivi-colis?listeNumerosLT=$tcode&langue=en&_=1609175933643";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Qapla - DDZZ</title>
    <style>
        html, body{
            padding:0;
            margin:0;
        }
        h1,p{
            margin: 2rem;
        }
        pre{
            background:#ddd;
            padding:1rem;
            max-width:100%;
            overflow:auto;
            max-height:calc(100vh - 15rem);
        }
    </style>
</head>

<body>
    

<?php


echo "<h1>Test Qapla - web scraping</h1>\n";
echo "<p>web scraping without checks and validates</p>\n";
?>
<form>
code truck <input name="code">
<button>Find</button>
</form>
<?php
echo "<hr/><pre>\n";

if(!$tcode)
    echo "<p><i>* find code</i></p>";
else
    print_r(
        array_reverse(
            json_decode(
                parse_optimize(
                    fetch_resp($ajaxUrl, $url, $host)
                )
            )
        )
    );




function fetch_resp($ajaxUrl, $url, $host){
    $ch = curl_init();
    $optArray = array(
        CURLOPT_URL => $ajaxUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_AUTOREFERER=>false,
        // headers args needed
        CURLOPT_HTTPHEADER => [
            "Referer:$url",
            "Host:$host"
        ]
    );
    curl_setopt_array($ch, $optArray);
    $result = curl_exec($ch);
    $result = json_decode($result)->tab;
    preg_match_all('/<td.*?>(.*?)<\/td>/m', $result, $matches, PREG_SET_ORDER, 0);
    return $matches;
}

function parse_optimize($full_str){
    $opt_str = [];
    foreach($full_str as $v)
            $opt_str[] = trim(preg_replace('/\s{2,}/',' ',(html_entity_decode(str_replace('<br />',' ',$v[1])))));
        
    $data = [];
    $pass = 0;
    $keys_content = ['','datetime','country&status','comment'];

    for($i=0;$i<count($opt_str); $i++)
        if(ord($opt_str[$i][0])==194){ // new line
            $pass++;
            reset($keys_content);
        }else
            // echo $pass.' '.next($keys_content).' '.$opt_str[$i]."\n";
            $data[$pass][next($keys_content)] = $opt_str[$i];
    return json_encode($data);
}
 ?>

</body>
</html>

